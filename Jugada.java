package tateti;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Jugada {
 
   String C1=" ", C2=" ", C3=" ", C4=" ", C5=" ", C6=" ", C7=" ", C8=" ", C9=" ", C10=" ";
   int turno=0;
   int j1=0, j2=0;
   
   
   
   public int getTurno() {
	return turno;
    }

   public void setTurno(int turno) {
	this.turno = turno;
   }
     
   
   public int getJ1() {
	return j1;
   }

   public void setJ1(int j1) {
	this.j1 = j1;
    }

   public int getJ2() {
	return j2;
   }

   public void setJ2(int j2) {
	this.j2 = j2;
   }

   
   //Metodos
   
   
   protected void Opcion(int l, int t) {
	   if((l==1)&&(t==0)) {
		   C1="x";
		}
	   else if((l==1) && (t==1)) {
		   C1="o";
	   }
	   else if((l==2) && (t==0)) {
		   C2="x";
	   }
	   else if((l==2) && (t==1)) {
		   C2="o";
	   }
	   else if((l==3) && (t==0)) {
		   C3="x";
	   }
	   else if((l==3) && (t==1)) {
		   C3="o";
	   }
	   else if((l==4) && (t==0)) {
		   C4="x";
	   }
	   else if((l==4) && (t==1)) {
		   C4="o";
	   }
	   
	   else if((l==5) && (t==0)) {
		   C5="x";
	   }
	   else if((l==5) && (t==1)) {
		   C5="o";
	   }
	   else if((l==6) && (t==0)) {
		   C6="x";
	   }
	   else if((l==6) && (t==1)) {
		   C6="o";
	   }
	   else if((l==7) && (t==0)) {
		   C7="x";
	   }
	   else if((l==7) && (t==1)) {
		   C7="o";
	   }
	   else if((l==8) && (t==0)) {
		   C8="x";
	   }
	   else if((l==8) && (t==1)) {
		   C8="x";
	   }
	   else if((l==9) && (t==0)) {
		   C9="x";
	   }
	   else if((l==9) && (t==1)) {
		   C9="o";
	   }
	   Ganador();
   }
      
   protected void Ganador() {
	   //cuando gana X en lados horizontales
	   if ((C1.equals("x"))&&(C2.equals("x"))&&(C3.equals("x"))) {
		   j1++;
		   this.setJ1(j1);//
		   	   
		   JOptionPane.showMessageDialog(null, "Jugador 1 Gana!");
	   }
	   
	   if ((C4.equals("x"))&&(C5.equals("x"))&&(C6.equals("x"))) {
		   j1++;
	   
		   JOptionPane.showMessageDialog(null, "Jugador 1 Gana!");
	   }
	   if ((C7.equals("x"))&&(C8.equals("x"))&&(C9.equals("x"))) {
		   j1++;
	   
		   JOptionPane.showMessageDialog(null, "Jugador 1 Gana!");
	   }
	   //Cuando gana X en forma toroidal
	   if ((C2.equals("x"))&&(C4.equals("x"))&&(C9.equals("x"))) {
		   j1++;
	   
		   JOptionPane.showMessageDialog(null, "Jugador 1 Gana!");
	   }
	   if ((C2.equals("x"))&&(C6.equals("x"))&&(C7.equals("x"))) {
		   j1++;
	   
		   JOptionPane.showMessageDialog(null, "Jugador 1 Gana!");
	   }
	   
	   //Cuando gana X en forma vertical
	   if ((C1.equals("x"))&&(C4.equals("x"))&&(C7.equals("x"))) {
		   j1++;
	   
		   JOptionPane.showMessageDialog(null, "Jugador 1 Gana!");
	   }
	   if ((C2.equals("x"))&&(C5.equals("x"))&&(C8.equals("x"))) {
		   j1++;
	   
		   JOptionPane.showMessageDialog(null, "Jugador 1 Gana!");
	   }
	   if ((C3.equals("x"))&&(C6.equals("x"))&&(C9.equals("x"))) {
		   j1++;
	   
		   JOptionPane.showMessageDialog(null, "Jugador 1 Gana!");
	   }
	   //Cuando gana O
	   
	 //cuando gana X en lados horizontales
	   if ((C1.equals("o"))&&(C2.equals("o"))&&(C3.equals("o"))) {
		   j2++;
	   
		   JOptionPane.showMessageDialog(null, "Jugador 2 Gana!");
	   }
	   																	//se podrian hacer 3 metodos (chequear pos horizontales, pos verticales, toroidal)
	   if ((C4.equals("o"))&&(C5.equals("o"))&&(C6.equals("o"))) {
		   j2++;
	   
		   JOptionPane.showMessageDialog(null, "Jugador 2 Gana!");
	   }
	   if ((C7.equals("o"))&&(C8.equals("o"))&&(C9.equals("o"))) {
		   j2++;
	   
		   JOptionPane.showMessageDialog(null, "Jugador 2 Gana!");
	   }
	   //Cuando gana X en forma toroidal
	   if ((C2.equals("o"))&&(C4.equals("o"))&&(C9.equals("o"))) {
		   j2++;
	   
		   JOptionPane.showMessageDialog(null, "Jugador 2 Gana!");
	   }
	   if ((C2.equals("o"))&&(C6.equals("o"))&&(C7.equals("o"))) {
		   j2++;
	   
		   JOptionPane.showMessageDialog(null, "Jugador 2 Gana!");
	   }
	   
	   //Cuando gana O en forma vertical
	   if ((C1.equals("o"))&&(C4.equals("o"))&&(C7.equals("o"))) {
		   j2++;
	   
		   JOptionPane.showMessageDialog(null, "Jugador 2 Gana!");
	   }
	   if ((C2.equals("o"))&&(C5.equals("o"))&&(C8.equals("o"))) {
		   j2++;
	   
		   JOptionPane.showMessageDialog(null, "Jugador 2 Gana!");
	   }
	   if ((C3.equals("o"))&&(C6.equals("o"))&&(C9.equals("o"))) {
		   j2++;
	   
		   JOptionPane.showMessageDialog(null, "Jugador 2 Gana!");
	   }
   }
		   
		   

           
   protected void jugar(int turno, JLabel label, int n) {
	  if(turno==0) {
		  label.setText("x");
		  Opcion(n, turno);
		  this.setTurno(1);;
		  
	 }
	  
	  else {
		  label.setText("o");
		  Opcion(n, turno);
		  this.setTurno(0);
	  }
    }
    
   protected void Limpiar(JLabel label) {
	   label.setText(" ");
		
   }
   
   protected void PuntajeX(JLabel label) { 
	   C10=String.valueOf(getJ1());
	   label.setText(C10);
   }
   protected void PuntajeO(JLabel label) { 
	   C10=String.valueOf(getJ2());
	   label.setText(C10);
   }

    
