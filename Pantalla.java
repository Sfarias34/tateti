package tateti;

import tateti.Jugada;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import java.awt.event.ComponentAdapter;


public class Pantalla extends javax.swing.JFrame  {
	
	private Jugada obj= new Jugada();
	private JFrame frame;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla window = new Pantalla();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel Label_1 = new JLabel("");
		Label_1.setHorizontalAlignment(SwingConstants.CENTER);
		Label_1.setForeground(Color.BLACK);
		Label_1.setFont(new Font("Tahoma", Font.BOLD, 50));
		Label_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				obj.jugar(obj.turno, Label_1 , 1);
			}
		});
		Label_1.setBounds(52, 21, 77, 76);
		frame.getContentPane().add(Label_1);
		
	
		JLabel Label_2 = new JLabel("");
		Label_2.setHorizontalAlignment(SwingConstants.CENTER);
		Label_2.setFont(new Font("Tahoma", Font.BOLD, 50));
		Label_2.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				obj.jugar(obj.turno, Label_2 , 2);
			}
		});
		Label_2.setBounds(177, 21, 77, 76);
		frame.getContentPane().add(Label_2);
		
		JLabel Label_3 = new JLabel("");
		Label_3.setFont(new Font("Tahoma", Font.BOLD, 50));
		Label_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				obj.jugar(obj.turno, Label_3 , 3);
			}
		});
		Label_3.setHorizontalAlignment(SwingConstants.CENTER);
		Label_3.setBounds(297, 21, 77, 76);
		frame.getContentPane().add(Label_3);
		
		JLabel Label_4 = new JLabel("");
		Label_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				obj.jugar(obj.turno, Label_4 , 4);
			}
		});
		Label_4.setFont(new Font("Tahoma", Font.BOLD, 50));
		Label_4.setHorizontalAlignment(SwingConstants.CENTER);
		Label_4.setBounds(52, 132, 77, 76);
		frame.getContentPane().add(Label_4);
		
		JLabel Label_5 = new JLabel("");
		Label_5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				obj.jugar(obj.turno, Label_5 , 5);
			}
		});
		Label_5.setFont(new Font("Tahoma", Font.BOLD, 50));
		Label_5.setHorizontalAlignment(SwingConstants.CENTER);
		Label_5.setBounds(176, 132, 77, 76);
		frame.getContentPane().add(Label_5);
		
		JLabel Label_6 = new JLabel("");
		Label_6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				obj.jugar(obj.turno, Label_6 , 6);
			}
		});
		Label_6.setFont(new Font("Tahoma", Font.BOLD, 50));
		Label_6.setHorizontalAlignment(SwingConstants.CENTER);
		Label_6.setBounds(297, 132, 77, 76);
		frame.getContentPane().add(Label_6);
		
		JLabel Label_7 = new JLabel("");
		Label_7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				obj.jugar(obj.turno, Label_7 , 7);
			}
		});
		Label_7.setFont(new Font("Tahoma", Font.BOLD, 50));
		Label_7.setHorizontalAlignment(SwingConstants.CENTER);
		Label_7.setBounds(52, 257, 77, 76);
		frame.getContentPane().add(Label_7);
		
		JLabel Label_8 = new JLabel("");
		Label_8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				obj.jugar(obj.turno, Label_8 , 8);
			}
		});
		Label_8.setFont(new Font("Tahoma", Font.BOLD, 50));
		Label_8.setHorizontalAlignment(SwingConstants.CENTER);
		Label_8.setBounds(176, 257, 77, 76);
		frame.getContentPane().add(Label_8);
		
		JLabel Label_9 = new JLabel("");
		Label_9.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				obj.jugar(obj.turno, Label_9 , 9);
			}
		});
		Label_9.setFont(new Font("Tahoma", Font.BOLD, 50));
		Label_9.setHorizontalAlignment(SwingConstants.CENTER);
		Label_9.setBounds(297, 257, 77, 76);
		frame.getContentPane().add(Label_9);
		
		
		JLabel lblJugadorX = new JLabel("Jugador X: ");
		lblJugadorX.setHorizontalAlignment(SwingConstants.CENTER);
		lblJugadorX.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblJugadorX.setBounds(10, 375, 77, 19);
		frame.getContentPane().add(lblJugadorX);
		
		JLabel lblNewLabel = new JLabel("Jugador O:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblNewLabel.setBounds(10, 416, 77, 19);
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnNewButton = new JButton("SALIR");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frame= new JFrame("SALIR"); 
				if(JOptionPane.showConfirmDialog(frame, "Quieres salir del juego?", "TA TE TI", 
						JOptionPane.YES_NO_OPTION)== JOptionPane.YES_NO_OPTION) {
								System.exit(0);
				}
		      	else {
				return;
			}
			}
		});
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnNewButton.setBounds(257, 421, 110, 23);
		frame.getContentPane().add(btnNewButton);
		
		JLabel Label_10 = new JLabel("0");
		Label_10.addComponentListener(new ComponentAdapter() {
		
		});
		Label_10.setFont(new Font("Times New Roman", Font.BOLD, 13));
		Label_10.setHorizontalAlignment(SwingConstants.CENTER);
		Label_10.setBounds(101, 377, 46, 14);
		frame.getContentPane().add(Label_10);
		
		JLabel Label_11 = new JLabel("0");
		Label_11.setFont(new Font("Times New Roman", Font.BOLD, 13));
		Label_11.setHorizontalAlignment(SwingConstants.CENTER);
		Label_11.setBounds(101, 418, 46, 14);
		frame.getContentPane().add(Label_11);
		
		JButton btnReiniciar = new JButton("REINICIAR");
		btnReiniciar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				obj.Limpiar(Label_1);
				obj.Limpiar(Label_2);
				obj.Limpiar(Label_3);
				obj.Limpiar(Label_4);
				obj.Limpiar(Label_5);
				obj.Limpiar(Label_6);
				obj.Limpiar(Label_7);
				obj.Limpiar(Label_8);
				obj.Limpiar(Label_9);
				obj.PuntajeX(Label_10);
				obj.PuntajeO(Label_11);
			}
		});
		btnReiniciar.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnReiniciar.setBounds(257, 387, 110, 23);
		frame.getContentPane().add(btnReiniciar);
		JLabel Fondo = new JLabel("");
		Fondo.setIcon(new ImageIcon(Pantalla.class.getResource("/imagenes/fondo.jpg")));
		Fondo.setBounds(0, 0, 480, 450);
		frame.getContentPane().add(Fondo);
	}
}
